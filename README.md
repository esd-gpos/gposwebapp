# gpos-webapp

## Project setup - Build the image and fire up the container: use http://localhost:3000/
```
docker-compose up -d --build
```

### Bring down the container
```
docker-compose stop
```

