FROM node:11.1-alpine 
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

# start app
CMD ["npm", "run", "serve"]